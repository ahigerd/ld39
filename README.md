![Depths Disconnected](https://static.jam.vg/content/320/3/z/7e89.png)

Ludum Dare 39
-------------
**Theme:** Running Out of Power

**Team:** [Adam Higerd](https://ldjam.com/users/coda-highland) and [Austin Allman](https://ldjam.com/users/drazil100)

Depths Disconnected
===================
Explore as a robot through a mining facility that has long since been abandoned in search of treasure! Avoid countless dangers such as bats, bats, and you guessed it, more bats! Be careful though as you have a short battery life and must stay charged to survive!

Downloads
---------
* [Windows](https://drive.google.com/open?id=0B3uG1VM-57B0OV9hWVRsMVI2Wk0)
* [Mac](https://drive.google.com/open?id=0B3uG1VM-57B0cTl4TzN2eGJqRFE)

Controls
--------
* Move: Arrow Keys
* Jump: Space
* Fire: Left Alt
* Pause: Escape

Credits
-------
Programming: [Adam Higerd](https://ldjam.com/users/coda-highland) and [Austin Allman](https://ldjam.com/users/drazil100)

Sound effects and design: [Adam Higerd](https://ldjam.com/users/coda-highland)

Character sprite base: [Platformer Sprites](https://opengameart.org/content/platformer-sprites) by yd

Crystal sprite: [Rotating Crystal Animation](https://opengameart.org/content/rotating-crystal-animation-8-step) by qubodup

Tileset sources:

* [OPP2017 - Cave and Mine Cart](https://opengameart.org/content/opp2017-cave-and-mine-cart) by Hapiel
* [Sci-Fi Platform Tiles](https://opengameart.org/content/sci-fi-platform-tiles) by Eris
* [Extension for Sci-Fi Platformer Tiles](https://opengameart.org/content/extension-for-sci-fi-platformer-tiles-32x32) by rubberduck

Background music: Various pieces from [Soundimage.org](http://soundimage.org) by Eric Matyas

License
=======
Copyright (c) 2017 Adam Higerd and Austin Allman

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

