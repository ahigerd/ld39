﻿// Based on Unity built-in shader source. Copyright (c) 2016 Unity Technologies. MIT license (see license.txt)
// Conversion functions from http://www.chilliant.com/rgb2hsv.html
Shader "Sprites/CheckpointBackground"
{
    Properties
    {
        [PerRendererData] _MainTex ("Sprite Texture", 2D) = "white" {}
        _HueShifts ("Hue shifts", Vector) = (0,0,0,0)
        _SaturationShifts ("Saturation shifts", Vector) = (0,0,0,0)
        _ValueShifts ("Value shifts", Vector) = (0,0,0,0)
        [MaterialToggle] PixelSnap ("Pixel snap", Float) = 0
        [HideInInspector] _RendererColor ("RendererColor", Color) = (1,1,1,1)
        [HideInInspector] _Flip ("Flip", Vector) = (1,1,1,1)
        [PerRendererData] _AlphaTex ("External Alpha", 2D) = "white" {}
        [PerRendererData] _EnableExternalAlpha ("Enable External Alpha", Float) = 0
    }

    SubShader
    {
        Tags
        {
            "Queue"="Transparent"
            "IgnoreProjector"="True"
            "RenderType"="Transparent"
            "PreviewType"="Plane"
            "CanUseSpriteAtlas"="True"
        }

        Cull Off
        Lighting Off
        ZWrite Off
        Blend One OneMinusSrcAlpha

        Pass
        {
        CGPROGRAM
            #pragma vertex SpriteVert
            #pragma fragment CheckpointBgFrag
            #pragma target 2.0
            #include "UnitySprites.cginc"

            fixed4 _HueShifts;
            fixed4 _SaturationShifts;
            fixed4 _ValueShifts;

            fixed Epsilon = 1e-10;
            fixed3 RGBtoHSV(in fixed3 RGB)
            {
              fixed4 P = (RGB.g < RGB.b) ? fixed4(RGB.bg, -1.0, 2.0/3.0) : fixed4(RGB.gb, 0.0, -1.0/3.0);
              fixed4 Q = (RGB.r < P.x) ? fixed4(P.xyw, RGB.r) : fixed4(RGB.r, P.yzx);
              fixed C = Q.x - min(Q.w, Q.y);
              fixed H = abs((Q.w - Q.y) / (6 * C + Epsilon) + Q.z);
              fixed S = C / (Q.x + Epsilon);
              return fixed3(H, S, Q.x);
            }

            fixed3 HSVtoRGB(in fixed3 HSV)
            {
              fixed R = abs(HSV.x * 6 - 3) - 2;
              fixed G = 1 - abs(HSV.x * 6 - 2);
              fixed B = 1 - abs(HSV.x * 6 - 4);
              return (clamp(fixed3(R,G,B), -1, 0) * HSV.y + 1) * HSV.z;
            }

            fixed easing(fixed v, fixed adj)
            {
              return (adj >= 0 ? (1.0f - v) : v) * adj;
            }

            fixed4 CheckpointBgFrag(v2f IN) : SV_Target
            {
                fixed4 c = SampleSpriteTexture (IN.texcoord);

                fixed4 weights = saturate(0.7f - fixed4(
                  distance(IN.texcoord, fixed2(0.5f, 0.0f)),
                  distance(IN.texcoord, fixed2(1.0f, 0.5f)),
                  distance(IN.texcoord, fixed2(0.5f, 1.0f)),
                  distance(IN.texcoord, fixed2(0.0f, 0.5f))
                ));
                fixed total = dot(weights, fixed4(1, 1, 1, 1));

                fixed3 hsv = RGBtoHSV(c.rgb);

                fixed hueShift = dot(_HueShifts, weights) / total;
                fixed satShift = dot(_SaturationShifts, weights) / total;
                fixed valShift = dot(_ValueShifts, weights) / total;

                hsv.x = (hsv.x + 1.0f + hueShift) % 1.0f;
                hsv.y += easing(hsv.y, satShift);
                hsv.z += easing(hsv.z, valShift);

                c.rgb = HSVtoRGB(hsv) * c.a;
                return c;
            }
        ENDCG
        }
    }
}
