using UnityEngine.Rendering;
using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public enum GameState {
	Title,
	Options,
	Spawning,
	Playing,
	Paused,
	GameOver,
};

public class GameManager : MonoBehaviour {
	private delegate void GameEventDelegate(LevelAwareBehaviour obj);

	private static GameEventDelegate makeDelegate(string eventName) {
		return (GameEventDelegate)Delegate.CreateDelegate(
			typeof(GameEventDelegate),
			null,
			typeof(LevelAwareBehaviour).GetMethod(eventName)
		);
	}

	public static List<LevelAwareBehaviour> behaviours = new List<LevelAwareBehaviour>();
	public static GameManager instance = null;
	public static int score = 0;
	public static int level = 0;
	public static int lives = 0;
	public static float energy = 1f;
	public static GameState state = GameState.Title;

	public Texture2D titleImage = null;

	private static Dictionary<string, GameEventDelegate> eventDelegates = new Dictionary<string, GameEventDelegate> {
		{ "OnGameStart", makeDelegate("OnGameStart") },
		{ "OnLevelStart", makeDelegate("OnLevelStart") },
		{ "OnPlayerSpawn", makeDelegate("OnPlayerSpawn") },
		{ "OnPlayerDeath", makeDelegate("OnPlayerDeath") },
		{ "OnGameOver", makeDelegate("OnGameOver") }
	};

	private void DispatchEvent(string eventName) {
		GameEventDelegate d = eventDelegates[eventName];
		foreach (LevelAwareBehaviour obj in new List<LevelAwareBehaviour>(behaviours)) {
			if (obj.isActiveAndEnabled) {
				d(obj);
			}
		}
	}

	void Awake() {
		instance = this;
		state = GameState.Title;
	}

	public void StartGame() {
		DispatchEvent("OnGameStart");
		level = 0;
		NewLevel();
	}

	public void NewLevel() {
		state = GameState.Spawning;
		level += 1;
		DispatchEvent("OnLevelStart");
	}

	public void StartLife() {
		lives -= 1;
		if (lives < 0) {
			RankScore (score);
			lives = 0;
			state = GameState.GameOver;
			DispatchEvent("OnGameOver");
		} else {
			state = GameState.Playing;
			DispatchEvent("OnPlayerSpawn");
		}
	}

	public void LoseLife() {
		state = GameState.Spawning;
		DispatchEvent("OnPlayerDeath");
	}
	
	private void RankScore(int newScore)
	{
		//LOADING
		int[] scores = new int[10];
		for(int i=0; i<10; i++) 
		{
			scores [i] = PlayerPrefs.GetInt ("score " + i, -1);
		}

		int score_ = newScore;

		if (score_ > 0) {
			if (score_ > scores [9])
				scores [9] = score_;
		
			for (int i = 8; i >= 0; i--) {
				if (scores [i + 1] > scores [i]) {
					int tmp = scores [i];
					scores [i] = scores [i + 1];
					scores [i + 1] = tmp;
				}
			}

			for (int i = 0; i < 10; i++) {
				PlayerPrefs.SetInt ("score " + i, scores [i]);
			}
		}
	}

	private int lastWidth;
	private int lastHeight;
	void Update() {
		if ((state == GameState.Title || state == GameState.GameOver) && Input.anyKeyDown) {
			StartGame();
		}

		if (
				(Input.GetKeyDown("return") || Input.GetKeyDown("enter")) &&
				(Input.GetKey("left alt") || Input.GetKey("right alt") || Input.GetKey("left cmd") || Input.GetKey("right cmd"))
		) {
			if (!Screen.fullScreen) {
				lastWidth = Screen.width;
				lastHeight = Screen.height;
				Resolution[] res = Screen.resolutions;
				Screen.SetResolution(res[res.Length - 1].width, res[res.Length - 1].height, true);
			} else {
				Screen.SetResolution(lastWidth, lastHeight, false);
			}
		}

		if (state == GameState.Playing && Input.GetButtonDown("Cancel")) {
			state = GameState.Paused;
		} else if (state == GameState.Paused && Input.anyKeyDown) {
			state = GameState.Playing;
		}
		Time.timeScale = (state == GameState.Paused) ? 0 : 1;

	}

	public static void DrawLabel(GUIStyle style, String text, float x, float y, float w, float h) {
		Rect bounds = new Rect(x * Screen.width, y * Screen.height, w * Screen.width, h * Screen.height);
		GUI.Label(bounds, text, style);
	}

	public static void DrawBox(GUIStyle style, String text, float x, float y, float w, float h) {
		Rect bounds = new Rect(x * Screen.width, y * Screen.height, w * Screen.width, h * Screen.height);
		GUI.Box(bounds, " ", style);
		GUI.Box(bounds, text, style);
	}

	public static void DrawTexture(Texture2D tex, float x, float y, float w, float h) {
		if (tex != null) {
			Rect bounds = new Rect(x * Screen.width, y * Screen.height, w * Screen.width, h * Screen.height);
			GUI.DrawTexture(bounds, tex, ScaleMode.ScaleToFit);
		}
	}

	void OnGUI() {
		GUIStyle boxStyle = new GUIStyle(GUI.skin.box);
		boxStyle.fontSize = Screen.height / 20;
		boxStyle.alignment = TextAnchor.MiddleRight;

		GUIStyle labelStyle = new GUIStyle(GUI.skin.label);
		labelStyle.fontSize = Screen.height / 16;
		labelStyle.alignment = TextAnchor.MiddleCenter;

		GUIStyle listStyle = new GUIStyle(labelStyle);
		listStyle.fontSize = boxStyle.fontSize;
		listStyle.alignment = TextAnchor.MiddleLeft;

		GUIStyle hudBoxStyle = new GUIStyle(GUI.skin.box);
		hudBoxStyle.fontSize = Screen.height / 30;
		hudBoxStyle.alignment = TextAnchor.MiddleRight;

		if (state == GameState.Paused) {
			DrawBox(boxStyle, " ", .3f, .3f, .4f, .4f);
			if ((int)(System.DateTime.Now.Millisecond) < 500) {
				DrawLabel(labelStyle, "PAUSED\n\nPress Any Key", .3f, .3f, .4f, .4f);
			}
		}

		if (state == GameState.Title || state == GameState.GameOver) {
			DrawBox(boxStyle, " ", .01f, .02f, .33f, .96f);
			DrawBox(boxStyle, " ", .37f, .02f, .59f, .1f);

			if ((int)(System.DateTime.Now.Millisecond) < 500) {
				DrawLabel(labelStyle, "Press Any Key to Start", .37f, .02f, .59f, .1f);
			}

			labelStyle.alignment = TextAnchor.UpperCenter;
			DrawLabel(labelStyle, "HIGH SCORES", .01f, .02f, .33f, .96f);

			for (int i = 0; i < 10; i++) {
				int s = PlayerPrefs.GetInt ("score " + i, -1);
				if (s < 0)
					continue;
				DrawLabel(listStyle, "" + (i+1) + ":    " + s, .02f, .02f + .085f * (i + 1), .31f, .085f);
			}

			if (titleImage != null) {
				DrawTexture(titleImage, .37f, .14f, .59f, .84f);
			}
				
		} else {
			DrawBox(hudBoxStyle, "Score: " + score, .004f, .002f, .2f, .05f);
			DrawBox(hudBoxStyle, "Lives: " + lives, .208f, .002f, .2f, .05f);
			DrawBox(hudBoxStyle, "Level: " + level, .412f, .002f, .2f, .05f);
			GUILayout.BeginArea (new Rect (Screen.width * 0.616f, Screen.height * 0.002f, Screen.width * 0.2f, Screen.height * 0.05f));
			GetComponent<Menu> ().GetMeter (Screen.width * 0.2f, Screen.height * 0.05f, energy, new Color (0.5f, 0.5f, 0.5f, 0.5f), new Color (0f, 1f, 0f));
			//DrawBox(hudBoxStyle, "Energy: " + Mathf.Ceil(energy * 100f) + "%", .616f, .002f, .2f, .05f);
			GUILayout.EndArea();
		}
	}
}
