using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public class CharacterCore : LevelAwareBehaviour {
	public static float PIXEL = 1f/16f;
	public static float GRAVITY = 20f;
	public float height = 1f;
	public float speed = 4f;
	public float jumpPower = 6f;
	public float layer = 0f;
	public Vector2 hitBoxSize = new Vector2(0.6f, 0.8f);
	public AudioClip deathSound = null;
	[HideInInspector]
	public MapRoom currentRoom;
	protected AudioSource audioSource;
	protected bool isGrounded;

	private TileMap _tileMap = null;
	public TileMap tileMap
	{
		get
		{
			if (_tileMap == null) {
				_tileMap = GameObject.Find("Map").GetComponent<TileMap>();
			}
			return _tileMap;
		}
	}

	public Vector2 origin
	{
		get
		{
			return WorldToMapPoint(transform.position);
		}
		set
		{
			transform.position = MapToWorldPoint(value, layer);
		}
	}

	public Vector2 midpoint
	{
		get
		{
			return origin - new Vector2(0, hitBoxSize.y / 2f);
		}
	}

	public Rect hitBox
	{
		get
		{
			return HitBoxAt(origin);
		}
	}

	public static Vector2 WorldToMapPoint(Vector3 point)
	{
		return new Vector2(point.x, -point.y);
	}

	public static Vector3 MapToWorldPoint(Vector2 point, float layer = 0f)
	{
		return new Vector3(point.x, -point.y, layer);
	}

	public Rect HitBoxAt(Vector2 pos)
	{
		return new Rect(pos.x - hitBoxSize.x / 2f, pos.y - hitBoxSize.y, hitBoxSize.x, hitBoxSize.y);
	}

	public virtual void Start ()
	{
		audioSource = gameObject.AddComponent<AudioSource>();
		SpriteAnimationManager sam = gameObject.GetComponent<SpriteAnimationManager>() as SpriteAnimationManager;
		if (sam) {
			sam.SwitchAnimation("Idle", true);
		}
		Rigidbody2D rigidbody = gameObject.AddComponent<Rigidbody2D>() as Rigidbody2D;
		rigidbody.bodyType = RigidbodyType2D.Kinematic;
		rigidbody.useFullKinematicContacts = true;
		BoxCollider2D collider = gameObject.AddComponent<BoxCollider2D>() as BoxCollider2D;
		collider.size = hitBoxSize;
		collider.offset = new Vector2(0f, hitBoxSize.y / 2);
		isGrounded = false;
	}

	public bool IsGrounded()
	{
		return isGrounded;
	}

	public virtual Vector2 Move (Vector2 v)
	{
		Vector2 delta = v * Time.deltaTime;
		Vector2 target = origin + delta;

		Rect oldHB = hitBox;
		Rect newHB = HitBoxAt(target);

		// First sweep horizontal motion
		if (v.x > 0 && (tileMap.IsSolid (newHB.xMax + PIXEL, oldHB.yMax - PIXEL, currentRoom) || tileMap.IsSolid (newHB.xMax + PIXEL, oldHB.yMin, currentRoom)))
		{
			v.x = 0;
			target.x = Mathf.Floor (newHB.xMax + PIXEL) - PIXEL - hitBoxSize.x / 2;
		}
		else if (v.x < 0 && (tileMap.IsSolid (newHB.xMin - PIXEL, oldHB.yMax - PIXEL, currentRoom) || tileMap.IsSolid (newHB.xMin - PIXEL, oldHB.yMin, currentRoom)))
		{
			v.x = 0;
			target.x = Mathf.Ceil (newHB.xMin - PIXEL) + PIXEL + hitBoxSize.x / 2;
		}

		// Then sweep vertical motion
		if (v.y > 0 && (tileMap.IsSolid(oldHB.xMin, newHB.yMax + PIXEL, currentRoom) || tileMap.IsSolid(oldHB.xMax, newHB.yMax + PIXEL, currentRoom))) {
			isGrounded = true;
			target.y = Mathf.Floor(target.y + PIXEL);
			v.y = 0;
		} else if (v.y < 0 && (tileMap.IsSolid(oldHB.xMin, newHB.yMin - PIXEL, currentRoom) || tileMap.IsSolid(oldHB.xMax, newHB.yMin - PIXEL, currentRoom))) {
			isGrounded = false;
			target.y = Mathf.Ceil(target.y - PIXEL);
			v.y = 0;
		} else {
			isGrounded = false;
		}

		origin = target;

		float roomX = transform.position.x - currentRoom.origin.x;
		float roomY = -(transform.position.y - currentRoom.origin.y);
		if (roomX < 0 && currentRoom.left != null) {
			currentRoom = currentRoom.left;
		} else if (roomX > MapRoom.ROOM_WIDTH && currentRoom.right != null) {
			currentRoom = currentRoom.right;
		} else if (roomY < 0 && currentRoom.above != null) {
			currentRoom = currentRoom.above;
		} else if (roomY > MapRoom.ROOM_HEIGHT && currentRoom.below != null) {
			currentRoom = currentRoom.below;
		}

		return v;
	}

	public void OnDrawGizmos()
	{
		Collider2D collider = gameObject.GetComponent<Collider2D>();
		if (collider) {
			DrawDebugRect(collider.bounds.min, collider.bounds.max);
		}
	}

	// Vector3 parameters in world space
	public static void DrawDebugRect(Vector3 bottomLeft, Vector3 topRight)
	{
		Vector3 topLeft = new Vector3(bottomLeft.x, topRight.y, 0);
		Vector3 bottomRight = new Vector3(topRight.x, bottomLeft.y, 0);
		Debug.DrawLine(bottomLeft, bottomRight, Color.red, 0, false);
		Debug.DrawLine(bottomRight, topRight, Color.green, 0, false);
		Debug.DrawLine(topRight, topLeft, Color.blue, 0, false);
		Debug.DrawLine(topLeft, bottomLeft, Color.yellow, 0, false);
	}

	/*public void OnCollisionEnter2D(Collision2D coll)
	{
		Debug.Log("Collision Enter");
	}*/

	public void Kill(bool destroy)
	{
		// Use the player's audio source because this one might get destroyed
		PlayerController player = GameObject.FindWithTag("Player").GetComponent<PlayerController>();
		player.audioSource.PlayOneShot(deathSound);
		if (destroy) {
			UnityEngine.GameObject.Destroy(gameObject);
		}
	}

	public static float Approach(float current, float target, float rate)
	{
		if (Mathf.Abs (current - target) < 0.1f)
			return target;
		float time = Time.deltaTime;
		float steps = time * 60;
		return (current - target) * Mathf.Pow (0.5f, rate * steps) + target;
	}
}
