﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System;

public class Menu : MonoBehaviour {

	//public Color windowColor;
	//public Color barBGColor;

	private Dictionary<string, Texture2D> textures = new Dictionary<string, Texture2D> ();

	private GUIStyle GetBackgroundStyle(Color color)
	{
		GUIStyle background = new GUIStyle ();
		background.stretchWidth = true;
		background.stretchHeight = true;
		if (!textures.ContainsKey (color.ToString()))
		{
			textures.Add (color.ToString(), new Texture2D (1, 1));
			textures[color.ToString()].SetPixel (0, 0, color);
			textures[color.ToString()].Apply ();
			//Debug.Log (color.ToString ());
		}

		background.normal.background = textures[color.ToString()];
		background.padding = new RectOffset(0, 0, 0, 0);
		background.margin = new RectOffset (0, 5, 0, 5);
		background.overflow = new RectOffset(0, 0, 0, 0);

		return background;
	}

	private GUIStyle GetWindowStyle(Color windowColor)
	{
		GUIStyle background = new GUIStyle ();
		background.stretchWidth = true;
		background.stretchHeight = true;
		if (!textures.ContainsKey (windowColor.ToString()))
		{
			textures.Add (windowColor.ToString(), new Texture2D (1, 1));
			textures[windowColor.ToString()].SetPixel (0, 0, windowColor);
			textures[windowColor.ToString()].Apply ();
			//Debug.Log (windowColor.ToString ());
		}

		background.normal.background = textures[windowColor.ToString()];
		background.overflow = new RectOffset(0, 0, 0, 0);
		background.padding = new RectOffset(10, 10, 10, 10);

		return background;
	}

	private GUIStyle GetBarStyle(Color color)
	{
		GUIStyle bar = new GUIStyle ();
		if (!textures.ContainsKey (color.ToString()))
		{
			textures.Add (color.ToString(), new Texture2D (1, 1));
			textures[color.ToString()].SetPixel (0, 0, color);
			textures[color.ToString()].Apply ();
			//Debug.Log (color.ToString ());
		}

		bar.normal.background = textures[color.ToString()];
		bar.margin = new RectOffset (0, 0, 0, 0);
		bar.stretchWidth = true;
		bar.stretchHeight = true;
		bar.overflow = new RectOffset(0, 0, 0, 0);

		return bar;
	}


	public void GetMeter(float width, float height, float value, Color backgroundColor, Color barColor, string text = "")
	{
		GUILayout.BeginVertical (GUILayout.MaxWidth(width));
		if (text != "") GUILayout.Label (text);
		GUILayout.BeginHorizontal (GetBackgroundStyle(backgroundColor), GUILayout.MinWidth(width), GUILayout.MaxHeight(height));
		GUILayout.Box("", GetBarStyle(barColor), GUILayout.Width(Mathf.Clamp01(value) * width), GUILayout.MaxHeight(height));
		GUILayout.EndHorizontal ();
		GUILayout.EndVertical ();
	}
}
