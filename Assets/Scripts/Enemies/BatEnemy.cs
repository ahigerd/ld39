using UnityEngine;

public class BatEnemy : EnemyCore {
	private bool moveLeft = true;

	public override void Start() {
		base.Start();
		moveLeft = UnityEngine.Random.value > 0.5;
		GetComponent<SpriteRenderer>().flipX = moveLeft;
	}

	void FixedUpdate() {
		Vector2 after = Move(new Vector2(moveLeft ? -8 : 8, 0));
		if (after.x == 0) {
			moveLeft = !moveLeft;
			GetComponent<SpriteRenderer>().flipX = moveLeft;
		}
	}
}
