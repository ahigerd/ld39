using UnityEngine;

class MusicTrack {
	public GameObject gameObject;
	public AudioSource audioSource;
	private MusicManager manager;
	private bool stopping = false;

	public MusicTrack(MusicManager manager, AudioClip clip) {
		this.manager = manager;
		gameObject = new GameObject();
		audioSource = gameObject.AddComponent<AudioSource>();
		audioSource.clip = clip;
		audioSource.loop = true;
		audioSource.mute = true;
		audioSource.volume = 0;
		audioSource.Play();
	}

	public void StartPlaying() {
		stopping = false;
		audioSource.mute = false;
	}

	public void StopPlaying() {
		stopping = true;
	}

	public void Tick(float deltaTime) {
		if (audioSource.mute) {
			return;
		}
		if (audioSource.volume < 1f && !stopping) {
			audioSource.volume += deltaTime * manager.volumeLevel;
			if (audioSource.volume > manager.volumeLevel) {
				audioSource.volume = manager.volumeLevel;
			}
		} else if (audioSource.volume > 0f && stopping) {
			audioSource.volume -= deltaTime * manager.volumeLevel;
			if (audioSource.volume <= 0f) {
				audioSource.volume = 0f;
				audioSource.mute = true;
			}
		}
	}
}

public class MusicManager : LevelAwareBehaviour {
	public AudioClip unpoweredRoom, poweredRoom, checkpoint, combat;
	public float volumeLevel = 0.7f;
	private MapRoom currentRoom = null;
	private MusicTrack unpoweredTrack, poweredTrack, checkpointTrack, combatTrack;
	private MusicTrack currentTrack = null;
	private PlayerController player = null;

	private void PlayTrack(MusicTrack track) {
		if (currentTrack == track) {
			return;
		}
		if (currentTrack != null) {
			currentTrack.StopPlaying();
		}
		currentTrack = track;
		currentTrack.StartPlaying();
	}

	public override void OnGameStart() {
		unpoweredTrack.audioSource.time = 0;
	}

	public override void OnPlayerSpawn() {
		currentRoom = null;
	}

	public override void OnPlayerDeath() {
		if (currentTrack != null) {
			currentTrack.StopPlaying();
		}
		currentTrack = null;
	}

	public override void OnGameOver() {
		PlayTrack(poweredTrack);
	}

	void Awake() {
		unpoweredTrack = new MusicTrack(this, unpoweredRoom);
		poweredTrack = new MusicTrack(this, poweredRoom);
		checkpointTrack = new MusicTrack(this, checkpoint);
		combatTrack = new MusicTrack(this, combat);
	}

	void Start() {
		player = GameObject.FindWithTag("Player").GetComponent<PlayerController>();
		PlayTrack(poweredTrack);
	}

	void Update() {
		if (GameManager.state == GameState.Playing && currentRoom != player.currentRoom) {
			currentRoom = player.currentRoom;
			if (currentRoom.roomType == RoomType.Checkpoint) {
				PlayTrack(checkpointTrack);
			} else if (currentRoom.powered > 0) {
				PlayTrack(poweredTrack);
			} else {
				// TODO: combat
				PlayTrack(combatTrack);
			}
		}
		unpoweredTrack.Tick(Time.deltaTime);
		poweredTrack.Tick(Time.deltaTime);
		checkpointTrack.Tick(Time.deltaTime);
		combatTrack.Tick(Time.deltaTime);
	}
}
