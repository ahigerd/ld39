﻿// Based on Unity built-in shader source. Copyright (c) 2016 Unity Technologies. MIT license (see license.txt)
// Conversion functions from http://www.chilliant.com/rgb2hsv.html
Shader "Sprites/Recolorable"
{
    Properties
    {
        [PerRendererData] _MainTex ("Sprite Texture", 2D) = "white" {}
        _HueShift ("Hue shift", Float) = 0
        _SaturationShift ("Saturation shift", Float) = 0
        _ValueShift ("Value shift", Float) = 0
        [MaterialToggle] PixelSnap ("Pixel snap", Float) = 0
        [HideInInspector] _RendererColor ("RendererColor", Color) = (1,1,1,1)
        [HideInInspector] _Flip ("Flip", Vector) = (1,1,1,1)
        [PerRendererData] _AlphaTex ("External Alpha", 2D) = "white" {}
        [PerRendererData] _EnableExternalAlpha ("Enable External Alpha", Float) = 0
    }

    SubShader
    {
        Tags
        {
            "Queue"="Transparent"
            "IgnoreProjector"="True"
            "RenderType"="Transparent"
            "PreviewType"="Plane"
            "CanUseSpriteAtlas"="True"
        }

        Cull Off
        Lighting Off
        ZWrite Off
        Blend One OneMinusSrcAlpha

        Pass
        {
        CGPROGRAM
            #pragma vertex SpriteVert
            #pragma fragment RecolorSpriteFrag
            #pragma target 2.0
            #pragma multi_compile_instancing
            #pragma multi_compile _ PIXELSNAP_ON
            #pragma multi_compile _ ETC1_EXTERNAL_ALPHA
            #include "UnitySprites.cginc"

            fixed _HueShift;
            fixed _SaturationShift;
            fixed _ValueShift;

            fixed3 HSVtoRGB(in fixed3 HSV)
            {
              fixed R = abs(HSV.x * 6 - 3) - 2;
              fixed G = 1 - abs(HSV.x * 6 - 2);
              fixed B = 1 - abs(HSV.x * 6 - 4);
              return (clamp(fixed3(R,G,B), -1, 0) * HSV.y + 1) * HSV.z;
            }

            fixed Epsilon = 1e-10;
            fixed3 RGBtoHSV(in fixed3 RGB)
            {
              fixed4 P = (RGB.g < RGB.b) ? fixed4(RGB.bg, -1.0, 2.0/3.0) : fixed4(RGB.gb, 0.0, -1.0/3.0);
              fixed4 Q = (RGB.r < P.x) ? fixed4(P.xyw, RGB.r) : fixed4(RGB.r, P.yzx);
              fixed C = Q.x - min(Q.w, Q.y);
              fixed H = abs((Q.w - Q.y) / (6 * C + Epsilon) + Q.z);
              fixed S = C / (Q.x + Epsilon);
              return fixed3(H, S, Q.x);
            }

            fixed easing(fixed v, fixed adj)
            {
              return (adj >= 0 ? (1.0f - v) : v) * adj;
            }

            fixed4 RecolorSpriteFrag(v2f IN) : SV_Target
            {
                fixed4 c = SampleSpriteTexture (IN.texcoord);
                if (c.a < 0.8) {
                  return 0;
                }
                fixed3 hsv = RGBtoHSV(c.rgb);
                hsv += fixed3(
                  1.0f + _HueShift,
                  easing(hsv.y, _SaturationShift),
                  easing(hsv.z, _ValueShift)
                );
                hsv.x %= 1.0;
                c.rgb = HSVtoRGB(hsv) * c.a;
                return c;
            }
        ENDCG
        }
    }
}
